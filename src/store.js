import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    items: [],
    loading: false,
    filter: Object,
  },
  mutations: {
    setItems(state, items) {
      state.items = items;
    },
    load(state, loading) {
      state.loading = loading;
    },
    filter(state, filter) {
      state.filter = filter ? { ...state.filter, ...filter } : {};
    }
  },
  actions: {
    getItems({ commit }) {
      commit('load', true);

      axios
        .get('//adm.zdravproduct.com/api/v1/objects')
        .then(response => {
          commit('setItems', response.data.data);
        })
        .catch(error => {
          console.log(error);
          commit('load', false);
        })
        .finally(() => {
          commit('load', false);
        });
    },
    filter({ commit }, filter) {
      commit('filter', filter);
    }
  }
})
