module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  productionSourceMap: false,
  css: {
    modules: true,
    loaderOptions: {
      sass: {
        data: `@import "@/scss/variables.scss";`
      }
    }
  }
}
